'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose') 
const Product = require('./modeo/product')
const app = express()
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

app.get('/hola', (req,res)=>{
    res.status(200).send({message:"bienvenido"})
})

app.get('/api/product',(req,res)=>{
    res.status(200).send('aqui devolveremos los prodructos')
})

app.get('/api/product/:productId',(req,res)=>{

    let ProductId = req.params.productId
    Product.findById(ProductId,(err,product)=>{
        if(err) return res.status(500).send({message:'error al relaizar peticion'})
        if(!product) return res.status(404).send({message:'error el producto no existe'}) 
        res.status(200).send({product})
    })
   
})

app.post('/api/product',(req,res)=>{

    let product = new Product()
    product.name = req.body.name
    product.picture = req.body.picture
    product.price = req.body.price
    product.category = req.body.category
    product.description = req.body.description

    product.save((err,productStore)=>{

        if(err) res.status(500).send(`Error base de datos> ${err}`)
        res.status(200).send({product:productStore})
    })

})
app.put('/api/product/:productId',(req,res)=>{
    let ProductId = req.params.productId 
    let update = req.body
    Product.findByIdAndUpdate(ProductId, update, (err,productUpdate)=>{
        if(err) res.status(500).send(`Error al actualizar: ${err}`)
        res.status(200).send({product: productUpdate})
    })

})


app.delete('/api/product/:productId',(req,res)=>{
    let ProductId = req.params.productId
    Product.findById(ProductId,(err, product)=>{
        if(err) res.status(500).send(`Error al borrar: ${err}`)
    })
    product.remove(err =>{
        if(err) res.status(500).send(`Error al borrar: ${err}`)
        res.status(200).send({message:'Ha sido eliminado'})
    })
})


mongoose.connect('mongodb+srv://danielbustos86:daniela1912@cluster0-wxfwq.mongodb.net/test?retryWrites=true&w=majority',(err,res)=>{
    if(err) throw err
    console.log('conexion establecida')

    app.listen(3000,()=>{
        console.log("esta corriendo en el puerto 3000")
    })
    
    
})


